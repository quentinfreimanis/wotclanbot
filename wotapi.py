import aiohttp
import os
import asyncio
import time

import config
from dotenv import load_dotenv


load_dotenv()
WG_APPID = os.getenv("WG_APPID")
START = time.monotonic()


class Wotapi:

    def __init__(self):
        client = aiohttp.ClientSession()
        self.client = RateLimiter(client)

    async def fetch(self, region, request):

        api_endpoint = config.config["wotapi_endpoints"][region]

        url = f"{api_endpoint}{request}&application_id={WG_APPID}"

        async with await self.client.get(url) as response:

            if response.status == 200:

                data = await response.json()

                if data["status"] == "ok":
                    return data
                else:
                    print(f"\nREQUEST FAILED\nREQUEST: {url}\nDATA: {data}\n")

                    # need a limit
                    await asyncio.sleep(1)
                    return await self.fetch(region, request)

            # untested
            else:
                print(f"\nREQUEST FAILED\nREQUEST: {url}\nDATA: {data}\n RESPONSE.STATUS: {response.status}")
                await asyncio.sleep(1)
                return await self.fetch(region, request)

    async def get_default_lang(self, region, language):
        if language == "":
            language = config.config["wotapi_default_lang"][region]
        return language

    async def players(self, region, search, fields="", language="", limit=100, stype=""):
        """Method returns partial list of players. The list is filtered by initial characters of user name and sorted alphabetically"""

        language = await self.get_default_lang(region, language)

        request = f"account/list/?search={search}&fields={fields}&language={language}&limit={limit}&type={stype}"

        return await self.fetch(region, request)

    async def clans(self, region, search="", fields="", language="", limit=100, page_no=1):
        """Method searches through clans and sorts them in a specified order"""

        language = await self.get_default_lang(region, language)

        request = f"clans/list/?search={search}&fields={fields}&language={language}&limit={limit}&page_no={page_no}"

        return await self.fetch(region, request)

    async def clan_details(self, region, clan_id, access_token="", extra="", fields="", language="", members_key=""):
        """Method returns detailed clan information"""

        language = await self.get_default_lang(region, language)

        request = f"clans/info/?clan_id={clan_id}&access_token={access_token}&extra={extra}&fields={fields}&language={language}&members_key={members_key}"

        return await self.fetch(region, request)


    async def player_pdata(self, region, account_id, access_token="", extra="", fields="", language=""):
        """Method returns player details"""

        language = await self.get_default_lang(region, language)

        request = f"account/info/?account_id={account_id}&access_token={access_token}&extra={extra}&fields={fields}&language={language}"

        return await self.fetch(region, request)

    async def clan_battles(self, region, clan_id, fields="", language="", limit="", page_no=""):
        """Method returns list of clan's battles on the Global Map"""

        language = await self.get_default_lang(region, language)

        request = f"globalmap/clanbattles/?clan_id={clan_id}&fields={fields}&language={language}&limit={limit}&page_no={page_no}"

        return await self.fetch(region, request)

    async def clan_ratings(self, region, clan_id, date="", fields="", language=""):
        """Method returns clan ratings by specified IDs"""

        language = await self.get_default_lang(region, language)

        request = f"clanratings/clans/?clan_id={clan_id}&date={date}&fields={fields}&language={language}"

        return await self.fetch(region, request)

    async def provinces(self, region, front_id, arena_id="", daily_revenue_gte="", daily_revenue_lte="", fields="", landing_type="", language="", limit=100, order_by="", page_no="", prime_hour="", province_id=""):
        """Method returns information about the Global Map provinces"""

        language = await self.get_default_lang(region, language)

        request = f"globalmap/provinces/?front_id={front_id}&arena_id={arena_id}&daily_revenue_gte={daily_revenue_gte}&daily_revenue_lte={daily_revenue_lte}&fields={fields}&landing_type={landing_type}&language={language}&limit={limit}&order_by={order_by}&page_no={page_no}&prime_hour={prime_hour}&province_id={province_id}"

        return await self.fetch(region, request)

    async def fronts(self, region, fields="", front_id="", language="", limit="", page_no=""):
        """Method returns information about the Global Map Fronts"""

        language = await self.get_default_lang(region, language)
        
        request = f"globalmap/fronts/?fields={fields}&front_id={front_id}&language={language}&limit={limit}&page_no={page_no}"

        return await self.fetch(region, request)

class RateLimiter:
    """Rate limits an HTTP client that would make get() and post() calls.
    Calls are rate-limited by host.
    https://quentin.pradet.me/blog/how-do-you-rate-limit-calls-with-aiohttp.html
    This class is not thread-safe."""
    RATE = config.config["wotapi_rates"]["REQ_RATE"]
    MAX_TOKENS = config.config["wotapi_rates"]["MAX_TOKENS"]

    def __init__(self, client):
        self.client = client
        self.tokens = self.MAX_TOKENS
        self.updated_at = time.monotonic()

    async def get(self, *args, **kwargs):
        await self.wait_for_token()
        now = time.monotonic() - START
        return self.client.get(*args, **kwargs)

    async def wait_for_token(self):
        while self.tokens < 1:
            self.add_new_tokens()
            await asyncio.sleep(0.1)
        self.tokens -= 1

    def add_new_tokens(self):
        now = time.monotonic()
        time_since_update = now - self.updated_at
        new_tokens = time_since_update * self.RATE
        if self.tokens + new_tokens >= 1:
            self.tokens = min(self.tokens + new_tokens, self.MAX_TOKENS)
            self.updated_at = now
