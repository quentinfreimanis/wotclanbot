
import discord
from discord.ext import commands

import random
import asyncio
import os
import aiopg
from dotenv import load_dotenv

import config
from database import Database
from common import Bot


load_dotenv()
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")

description = """World of Tanks (PC) clan discord server helper"""

intents = discord.Intents.default()
intents.members = True

async def create_bot():
        pool = await aiopg.create_pool(database=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT)
        bot = Bot(pool=pool, command_prefix=get_prefix, description=description, intents=intents)
        return bot

async def run(bot):

    async with bot.pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute("SELECT version();")
            record = await cur.fetchone()

    print("\n" + "-" * 6)
    print(f"Connected to {record}")
    print("-" * 6 + "\n")

    for filename in os.listdir("./cogs"):
        if filename.endswith(".py"):
            bot.load_extension(f"cogs.{filename[:-3]}")
            print(f"{filename[:-3]} cog loaded")

    await bot.start(DISCORD_TOKEN)

async def stop_bot(bot):
    await bot.wot.client.client.close()
    bot.pool.close()
    await bot.pool.wait_closed()
    await bot.close()

async def get_prefix(bot, message):
    """per server prefixes."""

    # Check to see if we are outside of a guild. e.g DM's etc.
    if not message.guild:
        return ""

    id = message.guild.id
    default_prefix = config.config["default_prefix"]

    prefix = (await bot.dbobj.get_guild_data(id))[1]

    if prefix == None:
        prefix = default_prefix

    return prefix

if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    bot = loop.run_until_complete(create_bot())

    try:
        loop.run_until_complete(run(bot))
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(stop_bot(bot))
