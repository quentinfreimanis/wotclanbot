import discord
from discord.ext import tasks
from discord.ext import commands

import asyncio

import config
from common import BaseCommon

class BattleSchedule(BaseCommon, commands.Cog):

    def __init__(self, bot):
        super().__init__(bot)
        self.scheduleloop.start()

    def cog_unload(self):
        self.scheduleloop.cancel()

    @tasks.loop(minutes=config.config["schedule_freq"])
    async def scheduleloop(self):
        prov_tasks = []
        province_data = {}
        for region in config.config["regions"]:
            province_data[region] = {}
            fronts = (await self.wot.fronts(region, fields="front_id,is_active"))["data"]
            for front in fronts:
                front_id = front["front_id"]
                if front["is_active"] == True:
                    province_data[region][front_id] = []
                    prov_tasks.append(self.get_provinces(region, front_id))

        prov_results = await asyncio.gather(*prov_tasks)

        for provinces in prov_results:
            province_data[provinces["region"]][provinces["front_id"]].append(provinces["data"])

        guild_data = await self.dbobj.get_all_guild_data()

        guild_tasks = []
        for guild in guild_data:
            # check if guild is setup to receive scheduled battles
            if guild[2] != None and guild[3] != None and guild[5] != None:
                guild_tasks.append(self.send_upcomming_battle(guild, province_data))
                guild_tasks.append(self.send_clan_battles(guild, province_data))

        await asyncio.gather(*guild_tasks)

    async def send_upcomming_battle(self, guild, province_data):

        clan_battles = await self.get_prov_battles(guild[2], province_data[guild[3]])
        new_battles = await self.get_new_prov_battles(clan_battles, guild[4])

        channel = self.bot.get_channel(guild[5])

        for battle in new_battles:  
            embed = await self.gm_landing_embed(guild[3], guild[2], battle, guild[6])
            if embed != None:
                await channel.send(embed=embed)

        await self.dbobj.update_guild_data(guild[0], gm_schedule=clan_battles)

    async def send_clan_battles(self, guild, province_data):

        battles_data = (await self.wot.clan_battles(guild[3], clan_id=guild[2]))["data"]
        old_battles = guild[7]
        new_battles = await self.get_new_clan_battles(battles_data, old_battles)
        channel = self.bot.get_channel(guild[5])
        for battle in new_battles:
            for front in province_data[guild[3]]:
                for provinces in province_data[guild[3]][front]:
                    for province in provinces:
                        if province["front_id"] == battle["front_id"] and province["province_id"] == battle["province_id"]:
                            embed = await self.clan_battle_embed(guild[3], guild[2], battle, province, guild[6])
                            if embed != None:
                                await channel.send(embed=embed)
        await self.dbobj.update_guild_data(guild[0], clan_battles=battles_data)

    @scheduleloop.before_loop
    async def before_printer(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(BattleSchedule(bot))
