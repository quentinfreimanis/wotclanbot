import yaml

with open("config.yaml") as file:

    data = yaml.full_load(file)

    config = dict(data.items())